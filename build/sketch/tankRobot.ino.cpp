#include <Arduino.h>
#line 1 "/home/selene/tankRobot/tankRobot.ino"
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WebSocketserver.h>

ESP8266WebServer server;
WebSocketserver webSocket = WebSocketsServer(81);

uint8_t motorPin1 = 5;
uint8_t motorPin2 = 4;
uint8_t motorPin3 = 0;
uint8_t motorPin4 = 2;
uint16_t speed = 0;

char* ssid = "dork";
char* psk = "";

char webpage[] PROGMEM = R"=====(
<html>
    <head>
    <script>
        var Socket;
        function init(){
            Socket = new WebSocket('ws://' + window.location.hostname + ':81')
            Socket.onmessage = (event) => {
                document.getElementById('rxConsole').value += event.data;
            }
        }
        function move(direction){
            console.log(direction);
            Socket.send(direction);
        }
        function setSpeed(){
            console.log(document.getElementById("speedSlider").value);
            Socket.send('#' + document.getElementById("speedSlider").value);
        }
    </script>
    <style>
        .container{
            width: 100%;
            margin: 0;
            padding: 0;
        }
        .leftControl{
            width: 33%;
            margin: 0;
            padding: 0;
            float: left;
        }
        .middleControl{
            width: 33%;
            margin: 0;
            padding: 0;
            float: left;
        }
        .rightControl{
            width: 33%;
            margin: 0;
            padding: 0;
            float: left;
        }
        textarea{
            width:100%;
            height: 100%;
        }
    </style>
    </head>
    <body onload="javascript:init()">
        <div class="container">
            <div class="header">
            </div>
            <div class="leftControl">
                <table>
                    <tbody>
                        <tr>
                            <td></td>
                            <td><button onclick="move('F')">F</button></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><button onclick="move('L')">L</button></td>
                            <td></td>
                            <td><button onclick="move('R')">R</button></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><button onclick="move('B')">B</button></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="middleControl">
                <textarea id="rxConsole"></textarea>
            </div>
            <div class="rightControl">
                <input id="speedSlider" orient="vertical" type="range" value="0" step="1" min="0" max="1023" oninput="setSpeed()">
            </div>
        </div>
    </body>
</html>)=====";

void setup(){
    
    //Setup pin modes
    pinMode(motorPin1, OUTPUT);
    pinMode(motorPin2, OUTPUT);
    pinMode(motorPin3, OUTPUT);
    pinMode(motorPin4, OUTPUT);

    //Start Wifi/Serial/Socket

    Wifi.begin(ssid, psk);
    Serial.begin(115200);
    
    while(Wifi.status() != WL_CONNECTED){
        Serial.print(".");
        delay(500);
    }
    Serial.println("");
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());

    server.on("/", [](){
            server.send_P(200, "text/html", webpage);
            });
    server.begin();
    webSocket.begin();
    webpage.onEvent(webSocketEvent);
}

void loop(){
    webSocket.loop()
    server.handleClient();
    if(Serial.available()){
        char c[] = {(char)Serial.read()};
        webSocket.broadcastTXT(c, sizeof(c));
    }
}

void webSocketEvent(uint8_t num, WSType_t type, uint8_t * payload, size_t length){
    if(type == WStype_TEXT){
        if(payload[0] == "#"){
        speed = (uint16_t) strtol((const char *)) &payload[1], NULL, 10);
        Serial.print("Speed= ");
        Serial.println(speed);
        }
    }else{
        move(payload, speed);
        Serial.print("Move= ");
        Serial.println(payload);
    }

    for i = 0; i < length; i++)
        Serial.print((char) payload[1]);
        Serial.println;();
}

void move(uint8_t * payload, uint16_t speed){
    if(payload[0] == "F"){
        analogWrite(motorPin1, speed);
        digitalWrite(motorPin2, LOW);
        analogWrite(motorPin3, speed);
        digitalWrite(motorPin4, LOW);
    }else if(payload[0] == "L") {
        analogWrite(motorPin2, speed);
        digitalWrite(motorPin1, LOW);
        analogWrite(motorPin3, speed);
        digitalWrite(motorPin4, LOW);
    } else if(payload[0] == "R"){
        analogWrite(motorPin1, speed);
        digitalWrite(motorPin3, LOW);
        analogWrite(motorPin4, speed);
        digitalWrite(motorPin3, LOW);
    } else if(payload[0] == "B"){
        analogWrite(motorPin2, speed);
        digitalWrite(motorPin1, LOW);
        analogWrite(motorPin4, speed);
        digitalWrite(motorPin3, LOW);
    }
}

#line 1 "/home/selene/tankRobot/websocketstest.ino"





void setup(){
    Serial.begin(115200);
    Serial.println("Test");

};


void loop(){
    Serial.println("Test1");
};

