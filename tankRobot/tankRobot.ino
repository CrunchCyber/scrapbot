#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>
#include <DNSServer.h>
#include <TinyGPS++.h>

ESP8266WebServer server;
WebSocketsServer webSocket = WebSocketsServer(81);
DNSServer dnsServer;
TinyGPSPlus gps;

uint8_t motorPin1 = 5;
uint8_t motorPin2 = 4;
uint8_t motorPin3 = 0;
uint8_t motorPin4 = 2;
uint8_t tankSpeed = 0;


uint16_t previousMillis = 0;
uint16_t period = 5000;

String noFix = "No Fix";
char* ssid = "dork";
char* psk = "";

char* mySSID = "SCRAPBOT";

IPAddress ip(192,168,11,2);
IPAddress gateway(192,168,11,1);
IPAddress subnet(255,255,255,0);

const byte DNS_PORT = 53;

char webpage[] PROGMEM = R"=====(
<html>
    <head>
    <script>
        var Socket;
        var interval;
        function init(){
            Socket = new WebSocket('ws://' + window.location.hostname + ':81');
            Socket.onmessage = (event) => {                       
                event.data += "\\\n";
        	      document.getElementById('rxConsole').value += event.data;
               
         
	           }
        }
        //Start movement
        function move(direction){
          interval = setInterval(() => {
            //console.log(direction);
            Socket.send(direction);
          }, 100);
        }
        //End movement
        function end(){
           clearInterval(interval);
        }
        function setSpeed(){
            console.log(document.getElementById("speedSlider").value);
            Socket.send('#' + document.getElementById("speedSlider").value);
        }
    </script>
    <style>
        .container{
            width: 100%;
            margin: 0;
            padding: 0;
        }
        .leftControl{
            width: 33%;
            margin: 0;
            padding: 0;
            float: left;
        }
        .middleControl{
            width: 33%;
            margin: 0;
            padding: 0;
            float: left;
        }
        .rightControl{
            width: 33%;
            margin: 0;
            padding: 0;
            float: left;
        }
        textarea{
            width:100%;
            height: 100%;
        }
    </style>
    </head>
    <body onload="javascript:init();">
        <div class="container">
            <div class="header">
            </div>
            <div class="leftControl">
                <table>
                    <tbody>
                        <tr>
                            <td></td>
                            <td><button onmousedown="move('F')" onmouseup="end()">F</button></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><button onmousedown="move('L')" onmouseup="end()">L</button></td>
                            <td></td>
                            <td><button onmousedown="move('R')" onmouseup="end()">R</button></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><button onmousedown="move('B')" onmouseup="end()">B</button></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="middleControl">
                <textarea id="rxConsole"></textarea>
            </div>
            <div class="rightControl">
                <input id="speedSlider" orient="vertical" type="range" value="0" step="1" min="0" max="1023" oninput="setSpeed()">
            </div>
        </div>
    </body>
</html>)=====";

void setup(){
    
    //Setup pin modes
    pinMode(motorPin1, OUTPUT);
    pinMode(motorPin2, OUTPUT);
    pinMode(motorPin3, OUTPUT);
    pinMode(motorPin4, OUTPUT);
    
   
    //Start Wifi/Serial/Socket
    WiFi.mode(WIFI_AP_STA);
    WiFi.begin(ssid, psk);
    WiFi.softAPConfig(ip, gateway, subnet);
    WiFi.softAP(mySSID, psk);
   
    Serial.begin(9600);
 
    
    while(WiFi.status() != WL_CONNECTED){
        Serial.print(".");
        delay(500);
    }
    Serial.println("");
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());

    dnsServer.start(DNS_PORT, "*", ip);

    server.onNotFound([]() {
    	const char *metaRefreshStr = "<head><meta http-equiv=\"refresh\" content=\"0; url=http://192.168.11.2/\" /></head><body><p>redirecting...</p></body>";
        server.send(200, "text/html", metaRefreshStr);
    });

    server.on("/", [](){
            server.send_P(200, "text/html", webpage);
            });
    server.begin();
    
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);

}

void getGPSLocation(){

}

void loop(){
   
    webSocket.loop();
    dnsServer.processNextRequest();
    server.handleClient();
    
        while(Serial.available() > 0){
          gps.encode(Serial.read());
          if(gps.location.isUpdated()){
            String lat = String(gps.location.rawLat().deg);
            lat += String(gps.location.rawLat().negative ? "S" : "N");
            lat += String(gps.location.rawLat().billionths);

            String lng = String(gps.location.rawLng().deg);
            lng += String(gps.location.rawLng().negative ? "W" : "E");
            lng += String(gps.location.rawLng().billionths);
            
            String sat = String(gps.satellites.value());

            webSocket.broadcastTXT(lat);
            webSocket.broadcastTXT(lng);
            webSocket.broadcastTXT(sat);
        
          }
        }
        
          
}

void move(uint8_t * payload, uint16_t speed){
    if(payload[0] == 'F'){
        analogWrite(motorPin2, speed);
        digitalWrite(motorPin1, LOW);
        analogWrite(motorPin3, speed);
        digitalWrite(motorPin4, LOW);
        const char * msg = "Forward\n";
        webSocket.broadcastTXT(msg, sizeof(*msg));
         
        delay(100);
    }else if(payload[0] == 'L') {
        analogWrite(motorPin2, speed);
        digitalWrite(motorPin1, LOW);
        analogWrite(motorPin4, speed);
        digitalWrite(motorPin3, LOW);
        const char * msg = "Left\n";
        webSocket.broadcastTXT(msg, sizeof(*msg));
        delay(100);
    } else if(payload[0] == 'R'){
        analogWrite(motorPin1, speed);
        digitalWrite(motorPin2, LOW);
        analogWrite(motorPin3, speed);
        digitalWrite(motorPin4, LOW);
        const char * msg = "Right\n";
        webSocket.broadcastTXT(msg, sizeof(msg));
        delay(100);
    } else if(payload[0] == 'B'){
        analogWrite(motorPin1, speed);
        digitalWrite(motorPin2, LOW);
        analogWrite(motorPin4, speed);
        digitalWrite(motorPin3, LOW);
        const char * msg = "Backwards\n";
        webSocket.broadcastTXT(msg, sizeof(*msg));
        delay(100);
    }
    digitalWrite(motorPin1, LOW);
    digitalWrite(motorPin2, LOW);
    digitalWrite(motorPin3, LOW);
    digitalWrite(motorPin4, LOW);
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length){
  if(type == WStype_TEXT){
      if(payload[0] == '#'){
        tankSpeed = (uint16_t) strtol((const char *) &payload[1], NULL, 10);
        Serial.print("Speed= ");
        Serial.println(tankSpeed);
       
    }else{
        move(payload, tankSpeed);      
    }
  }
}
